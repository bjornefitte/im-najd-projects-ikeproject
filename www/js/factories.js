var url = 'http://planetexpress.home/ike/v1';

angular.module('starter.factories', [])
.factory('PostPath', function($resource) {
  return $resource(url.concat('/paths'));
})
.factory('PostComment', function($resource) {
  return $resource(url.concat('/paths/comment'));
})
.factory('PostRating', function($resource) {
  return $resource(url.concat('/paths/rate'));
})
.factory('SearchFactory', function($resource) {
  return $resource(url.concat('/paths/search/:query'), 
    {query: '@query' } ,
    { get: { method: 'GET' , isArray: true} }
    );

})
.factory('GetRating', function($resource) {
  return $resource(url.concat('/paths/id/:id/rating'
    ), 
  {id: '@id' } ,
  { get: { method: 'GET' , isArray: false} }
  );
})

.factory('GetPaths', function($resource) {
  return $resource(url.concat('/paths/coord/:lat/:long'
    ), 
  {lat: '@lat', long: '@long' } ,
  { get: { method: 'GET' , isArray: true} }
  );
})
.factory('GetComments', function($resource) {
  return $resource(url.concat('/paths/id/:id/comments'
    ), 
  {id: '@id' } ,
  { get: { method: 'GET' , isArray: true} }
  );
})
.factory('GetPath', function($resource) {
  return $resource(url.concat('/paths/id/:id'),
    {id: '@id'});
})