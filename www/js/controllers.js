angular.module('starter.controllers', ['ngResource'])

.controller('LoginCtrl', function($scope, auth, $state, store) {
  auth.signin({
    popup: true,
    // Make the widget non closeable
    standalone: true,
    // This asks for the refresh token
    // So that the user never has to log in again
    authParams: {
      scope: 'openid offline_access'
    }
  }, function(profile, idToken, accessToken, state, refreshToken) {
    store.set('profile', profile);
    store.set('token', idToken);
    store.set('refreshToken', refreshToken);
    $state.go('app.paths');
  }, function(error) {
    console.log("There was an error logging in", error);
  });
})

.controller('AppCtrl',function($scope, auth, $state, store) {
  $scope.name = auth.profile.name;
  $scope.userid =  auth.profile.user_id;
  $scope.social_medium = String($scope.userid).split("|")[0];
  $scope.image= auth.profile.picture;
  $scope.logout = function() {
    auth.signout();
    store.remove('token');
    store.remove('profile');
    store.remove('refreshToken');
    $state.go('login');
  }
})

.controller('PlaylistsCtrl', function($scope,$ionicLoading,$cordovaGeolocation, GetPaths) {

  $scope.loading = $ionicLoading.show({
          content: 'Getting current location...',
          showBackdrop: false
        });

  $cordovaGeolocation
    .getCurrentPosition()
    .then(function (position) {
      $scope.lat  = position.coords.latitude
      $scope.long = position.coords.longitude
      $scope.paths = GetPaths.get({'lat':$scope.lat,'long':$scope.long});
    $scope.loading = $ionicLoading.hide();
    }, function(err) {
      // error
    });

 // $scope.paths = GetPaths.get({'lat':$scope.lat,'long':$scope.long});
 

 /*
    { title: 'Reggae', id: 1 },
    { title: 'Chill', id: 2 },
    { title: 'Dubstep', id: 3 },
    { title: 'Indie', id: 4 },
    { title: 'Rap', id: 5 },
    { title: 'Cowbell', id: 6 }
    ];*/
    $scope.doRefresh = function() {
      $scope.paths = GetPaths.get({'lat':$scope.lat,'long':$scope.long});
      $scope.$broadcast('scroll.refreshComplete');
      $scope.$apply()
    };
  })

.controller('PathCtrl', function($scope,$state,$ionicPopup, $ionicModal,$stateParams,auth,PostRating,PostComment,GetRating,GetPath,mapService) {
  GetPath.get({'id':$stateParams.pathId}, 
    function (data) { 
     $scope.path = data;
     console.log('pats', JSON.stringify($scope.path));
     mapService.setLineString($scope.path.linestring);  
   });
  console.log($scope);

  GetRating.get({'id':$stateParams.pathId}, 
    function (data) { 
     $scope.rate = data.average;
     
   });
  console.log($scope);
  $scope.max = 5;
  $scope.isReadonly = false;

  $scope.hoveringOver = function(value) {
    $scope.overStar = value;
    $scope.percent = 100 * (value / $scope.max);

  };
  $scope.postCommentData = {};
  $scope.postRatingData={};
  $scope.postRatingData.pathid = parseInt($stateParams.pathId);
  $scope.postCommentData.pathid = parseInt($stateParams.pathId);
    $scope.postCommentData.userid = auth.profile.user_id; // to change.
    $scope.postCommentData.username = auth.profile.name;
    $scope.hoveringLeave = function(rate) {

     $scope.postRatingData.ratingvalue = rate;
     var post = new PostRating($scope.postRatingData);
     console.log(JSON.stringify($scope.postRatingData));
     post.$save();
     GetRating.get({'id':$stateParams.pathId}, 
          function (data) { 
          $scope.rate = data.average;
     
          });
   };

   $ionicModal.fromTemplateUrl('templates/comment.html', {
    scope: $scope,
    
    
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });
  $scope.open = function() {
    $scope.modal.show();
  };
  $scope.close = function() {
    $scope.modal.hide();
  };
  $scope.submitComment = function(){
    if (!scope.postCommentData.commenttext){
     var post = new PostComment($scope.postCommentData);
     console.log(JSON.stringify($scope.postCommentData));
     post.$save();
     $scope.modal.hide();
   } else {
      showAlertComment();
   }
  }
  var showAlertComment = function() {
   var alertPopup = $ionicPopup.alert({
     title: 'Comment is Empty',
     template: 'Please Write your Comment'
   });
   alertPopup.then(function(res) {
    
   });
 };
  //Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });
  // Execute action on hide modal
  $scope.$on('modal.hidden', function() {
    // Execute action
  });
  // Execute action on remove modal
  $scope.$on('modal.removed', function() {
    // Execute action
  });
})
.controller('CommentsCtrl', function($scope,$stateParams, GetComments) {
  $scope.comments = GetComments.get({'id':$stateParams.pathId});
  $scope.back = $stateParams.pathId;

})

.controller('viewMapCtrl', function($scope,$ionicLoading,$cordovaGeolocation,$stateParams, mapService) {
 var wkt = new Wkt.Wkt();
 wkt.read (mapService.getLineString());
 var middle = Math.floor(wkt.components.length/2);
 console.log();
 var pth;
 var map;
 $scope.back = $stateParams.pathId;
 //console.log(pth);


 
 function initialize() {
  console.log(wkt.components[middle].x,wkt.components[middle].y);
  var myLatlng = new google.maps.LatLng(wkt.components[middle].y,wkt.components[middle].x);

  var mapOptions = {
    center: myLatlng,
    zoom: 16,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  map = new google.maps.Map(document.getElementById("map"),
    mapOptions);

  pth = wkt.toObject(mapOptions);

  $scope.map = map;
}


google.maps.event.addDomListener(window, 'load', initialize);

initialize();
pth.setMap(map);



})

.controller('addCtrl', function($scope, $ionicModal,$state, $ionicPopup,$stateParams,auth,PostPath ,mapService) {

 $scope.postData = {};
 $scope.postData.path = mapService.getLineString();
   $scope.postData.userid = auth.profile.user_id; //toDoModify.
   $scope.postData.username = auth.profile.name;
   $scope.newPostPath = function() {
    console.log(JSON.stringify($scope.postData));
    var post = new PostPath($scope.postData);
    console.log(post);

    post.$save();
    showAlert();
  }
  
  var showAlert = function() {
   var alertPopup = $ionicPopup.alert({
     title: 'Path Added',
     template: 'The path you suggested has been added !'
   });
   alertPopup.then(function(res) {
     $state.go('app.paths');
   });
 };



})

.controller('addMapCtrl', function($scope,$state,$cordovaGeolocation,$ionicLoading,$ionicPopup,$stateParams, mapService) {
 $scope.loading = $ionicLoading.show({
          content: 'Getting current location...',
          showBackdrop: false
        });

 var wkt = new Wkt.Wkt();
 var poly;
 var map;
 var path;

 var showAlert = function() {
   var alertPopup = $ionicPopup.alert({
     title: 'Path Not Drawn',
     template: 'Please Draw the Path on the Map'
   });
   alertPopup.then(function(res) {
     $state.go('app.addMap');
   });
 };
 $scope.isEmpty = function (){
  if (!mapService.getLineString()) {
   showAlert();
 } else { 
  $state.go('app.addMap.addDetails');
}
}

function initialize() {
  var myLatlng = new google.maps.LatLng($scope.lat,$scope.long);

  var mapOptions = {
    center: myLatlng,
    zoom: 16,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  map = new google.maps.Map(document.getElementById("map"),
    mapOptions);
  var polyOptions = {
    strokeColor: '#000000',
    strokeOpacity: 1.0,
    strokeWeight: 3
  };
  poly = new google.maps.Polyline(polyOptions);
  poly.setMap(map);

  // Add a listener for the click event
  google.maps.event.addListener(map, 'mousedown', addLatLng);


  $scope.map = map;
}
function addLatLng(event) {

  path = poly.getPath();

     // Because path is an MVCArray, we can simply append a new coordinate
     // and it will automatically appear.
  path.push(event.latLng);
  console.log(path);
  var wkt = new Wkt.Wkt();
  wkt.fromObject(poly);
  mapService.setLineString( wkt.write());
  console.log ();
  // Add a new marker at the new plotted point on the polyline.
  var marker = new google.maps.Marker({
   position: event.latLng,
   title: '#' + path.getLength(),
   map: map
 });
}

$scope.removeLine = function () {
  
  
  mapService.setLineString(null);
 $state.transitionTo($state.current, $stateParams, {
    reload: true,
    inherit: false,
    notify: true
});
}

google.maps.event.addDomListener(window, 'load', initialize);
$cordovaGeolocation
    .getCurrentPosition()
    .then(function (position) {
      $scope.lat  = position.coords.latitude
      $scope.long = position.coords.longitude
      initialize();
    $scope.loading = $ionicLoading.hide();
    }, function(err) {
      // error
    });

})

.controller('SearchCtrl', function($scope, SearchFactory) {
  $scope.query = {
    string : ''
  };
  var doSearch = ionic.debounce(function(query) {
    console.log($scope.query.string);
    $scope.results = SearchFactory.get({'query':$scope.query.string});
  }, 500);
  $scope.search = function() {
    doSearch($scope.query);
  }
  /*  $scope.$watch('query.string', function($scope){console.log('aa')});*/
})





