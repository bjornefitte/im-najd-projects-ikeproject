// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'auth0',
  'angular-storage',
  'angular-jwt','ngCordova','ngResource','ui.bootstrap','starter.factories','starter.services','starter.directives','starter.controllers'])

.run(function($ionicPlatform) {
  FastClick.attach(document.body);
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider, authProvider, $httpProvider,
  jwtInterceptorProvider) {
  $stateProvider
  .state('login', {
    url: '/login',
    templateUrl: 'templates/login.html',
    controller: 'LoginCtrl',
  })
    .state('app', {
      url: "/app",
      abstract: true,
      templateUrl: "templates/menu.html",
      controller: 'AppCtrl',
      data: {
        requiresLogin: true
      }
    })

    .state('app.search', {
      url: "/search",
      views: {
        'menuContent' :{
          templateUrl: "templates/search.html",
          controller: 'SearchCtrl'
        }
      }
    })

    .state('app.addMap.addDetails', {
      url: "/addDetails",
      views: {
        'menuContent@app' :{
          templateUrl: "templates/addDetails.html",
           controller: 'addCtrl'
        }
      }
    })

    .state('app.addMap', {
      url: "/addMap",
      views: {
        'menuContent@app' :{
          templateUrl: "templates/addMap.html",
           controller: 'addMapCtrl'
        }
      }
    })

    .state('app.about', {
      url: "/about",
      views: {
        'menuContent' :{
          templateUrl: "templates/about.html"
        }
      }
    })
    .state('app.paths', {
      url: "/paths",
      views: {
        'menuContent' :{
          templateUrl: "templates/paths.html",
          controller: 'PlaylistsCtrl'
        }
      }
    })
   

    .state('app.single', {
      url: "/paths/:pathId",
      views: {
        'menuContent' :{
          templateUrl: "templates/path.html",
          controller: 'PathCtrl'
        }
      }
    })
   
.state('app.single.comments', {
      url: "/comments",
      views: {
        'menuContent@app' :{
          templateUrl: "templates/comments.html",
          controller: 'CommentsCtrl'
        }
      }
    })
.state('app.single.viewmap', {
      url: "/viewmap",
      views: {
        'menuContent@app' :{
          templateUrl: "templates/viewMap.html",
          controller: 'viewMapCtrl'
        }
      }
    })
    ;
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/paths');


authProvider.init({
    domain: 'zyklons.auth0.com',
    clientID: 'BPFFsk4DeO1bgVl6qdqi4SW1opUjunYv',
    loginState: 'login'
  });

jwtInterceptorProvider.tokenGetter = function(store, jwtHelper, auth) {
    var idToken = store.get('token');
    var refreshToken = store.get('refreshToken');
    if (!idToken || !refreshToken) {
      return null;
    }
    if (jwtHelper.isTokenExpired(idToken)) {
      return auth.refreshIdToken(refreshToken).then(function(idToken) {
        store.set('token', idToken);
        return idToken;
      });
    } else {
      return idToken;
    }
  }

   $httpProvider.interceptors.push('jwtInterceptor');

})
.run(function($rootScope, auth, store) {
  $rootScope.$on('$locationChangeStart', function() {
    if (!auth.isAuthenticated) {
      var token = store.get('token');
      if (token) {
        auth.authenticate(store.get('profile'), token);
      }
    }

  });
});

