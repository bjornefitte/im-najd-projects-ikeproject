
angular.module('starter.services', [])

.service('mapService', function() {
  var lineString;

  var setLineString = function(newObj) {
    console.log(newObj);
    lineString = newObj;

  }

  var getLineString = function(){
    console.log (lineString);
    return lineString;
  }

  return {
    getLineString: getLineString,
    setLineString : setLineString
  };

})
