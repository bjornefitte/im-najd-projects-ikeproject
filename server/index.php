<?php

ini_set("display_errors",1);
require '/usr/local/Slim/Slim/Slim.php';
//if($_POST)
//{
//    echo "asdfasdf";
//    exit;
//}
\Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();

 if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }

    // Access-Control headers are received during OPTIONS requests
    if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
            header("Access-Control-Allow-Methods: GET, POST, OPTIONS");         

        if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
            header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

        exit(0);
    }



$app->get('/paths/coord/:lat/:long', 'getPaths');
$app->get('/paths/id/:id',    'getPath');
$app->get('/paths/search/:query', 'findByName');
$app->get('/paths/id/:id/comments',    'getComments');
$app->get('/paths/id/:id/rating',    'getRating');
$app->post('/paths/', 'postPath');
$app->post('/paths/comment',    'postComment');
$app->post('/paths/rate',    'postRating');
$app->delete('/paths/id/:id',    'deletePath');

$app->run();

function getPath($id) {
    
    $sql = "SELECT  `pathid` , title, 
            TYPE , eta, description, specialguides, difficulty, ASTEXT( path ) as linestring , userid, username
            FROM paths
            WHERE pathid =:id";
    try {
        $dbCon = getConnection();
        $stmt = $dbCon->prepare($sql);  
        $stmt->bindParam("id", $id);
        //echo $sql;
        $stmt->execute();
        $user = $stmt->fetchObject();  

        $dbCon = null;
        echo json_encode($user); 
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}'; 
    }
}
function deletePath($id) {
    $sql = "DELETE FROM paths WHERE pathid=:id";
    try {
        $dbCon = getConnection();
        $stmt = $dbCon->prepare($sql);  
        $stmt->bindParam("id", $id);
        $status = $stmt->execute();
        $dbCon = null;
        echo json_encode($status);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}'; 
    }
}
function postComment() {
    global $app;

    $request = $app->request();
    $body = $request->getBody();
    $event = json_decode($body);
    $paramPathId = $event->pathid;
    $paramUserId = $event->userid;
    $paramCText = $event->commenttext;
    $paramUsername = $event->username;
    $sql = "INSERT INTO  `capstone_base`.`comments` (
    		`userid` ,
    		`pathid` ,
    		`commenttext`,
            `username`
    		)
			VALUES (
			:userid,  :pathid,  :commenttext, :username
			);";
    try {

        $dbCon = getConnection();
        
        $stmt = $dbCon->prepare($sql);  
        $stmt->bindParam("userid", $paramUserId);
        $stmt->bindParam("pathid", $paramPathId);
        $stmt->bindParam("username",$paramUsername);
        $stmt->bindValue("commenttext", $paramCText);
        $status = $stmt->execute();
        $dbCon = null;
        echo json_encode($status);
    } catch(PDOException $e) {
        echo '{"errorComment":{"text":'. $e->getMessage() .'}}'; 
    }
}

function postRating() {
    global $app;
    $request = $app->request();
    $body = $request->getBody();
    $event = json_decode($body);
    $paramPathId = $event->pathid;
    $paramRValue = $event->ratingvalue;
    
    
    $sql = "INSERT INTO ratings( pathid, ratingvalue ) 
    		VALUES (
    		:pathid, :ratingvalue
    		)";
    try {
        $dbCon = getConnection();
        $stmt = $dbCon->prepare($sql);  
        
        $stmt->bindParam("pathid", $paramPathId);
        $stmt->bindParam("ratingvalue", $paramRValue);
        $stmt->execute();
        $id = $paramPathId;
        $dbCon = null;
        getRating($id);
        
    } catch(PDOException $e) {
        echo '{"$errorPath":{"text":'. $e->getMessage() .'}}'; 
    }
}

function postPath() {
    global $app;
    $request = $app->request();
    $body = $request->getBody();
    $event = json_decode($body);
     // Getting parameter with names
    $paramTitle = $event->title; // Getting parameter with names
    $paramType = $event->type; // Getting parameter with names
    $paramEta = $event->eta; // Getting parameter with names
    $paramDesc = $event->description;
    $paramGuides = $event->specialguides; // Getting parameter with names
    $paramDiff = $event->difficulty; // Getting parameter with names
    $paramPath = $event->path; // Getting parameter with names
    $paramUserId = $event->userid;
     $paramUsername = $event->username;
    $sql = "INSERT INTO  `capstone_base`.`paths` (
    			`pathid` ,
    			`title` ,
    			`type` ,
    			`eta` ,
    			`description` ,
    			`specialguides` ,
    			`difficulty` ,
    			`path` ,
    			`userid`,
                `username`
    		)
			VALUES (
				NULL ,  :title,  :type ,  :eta,    :description, :specialguides, :difficulty, GEOMFROMTEXT(  :path ) ,  :userid , :username
			);";
    try {
        $dbCon = getConnection();
        $stmt = $dbCon->prepare($sql);  
        $stmt->bindParam("title", $paramTitle);
        $stmt->bindParam("type", $paramType);
        $stmt->bindParam("eta", $paramEta);
        $stmt->bindParam("description", $paramDesc);
        $stmt->bindParam("specialguides", $paramGuides);
        $stmt->bindParam("difficulty", $paramDiff);
        $stmt->bindParam("path", $paramPath);
        $stmt->bindParam("userid", $paramUserId);
        $stmt->bindParam("username", $paramUsername);
        $stmt->execute();
        $id = $dbCon->lastInsertId();
        $dbCon = null;
        getPath($id);
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}'; 
    }
}

function getPaths($lat,$long) {
	$sql = "SELECT pathid, title, eta, TYPE, difficulty,TRUNCATE( 111.045 * DEGREES( ACOS( COS( RADIANS( latpoint ) ) * COS( RADIANS( Y( STARTPOINT( path ) ) ) ) * COS( RADIANS( longpoint ) - RADIANS( X( STARTPOINT( path ) ) ) ) + SIN( RADIANS( latpoint ) ) * SIN( RADIANS( Y( STARTPOINT( path ) ) ) ) ) ) , 2)AS distance_in_km
			FROM paths
			JOIN (
				SELECT :lat AS latpoint, :long AS longpoint
			) AS p
			ORDER BY distance_in_km;";
	try {
		$dbCon = getConnection();
		$stmt = $dbCon->prepare($sql);  
		$stmt->bindParam("lat", $lat);
		$stmt->bindParam("long", $long);
		$stmt->execute();
		$user = $stmt->fetchAll(PDO::FETCH_ASSOC);  
		$dbCon = null;
		echo json_encode($user); 
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}



function getComments($id) {
	$sql = "SELECT * 
			FROM comments
			WHERE pathid = :id;";
    try {
        $dbCon = getConnection();
        $stmt = $dbCon->prepare($sql);  
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $user = $stmt->fetchAll(PDO::FETCH_ASSOC);  
        $dbCon = null;
        echo json_encode($user); 
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}'; 
    }
}
function getRating($id) {
	$sql = "SELECT AVG( ratingvalue ) as average
			FROM ratings
			WHERE pathid =:id";
    try {
        $dbCon = getConnection();
        $stmt = $dbCon->prepare($sql);  
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $user = $stmt->fetchObject();  
        $dbCon = null;
        echo json_encode($user); 
    } catch(PDOException $e) {
        echo '{"error":{"text":'. $e->getMessage() .'}}'; 
    }
}

function findByName($query) {
	$sql = "SELECT pathid, title, 
			TYPE , eta,difficulty
			FROM paths
			WHERE title LIKE  concat('%', :query, '%')
			ORDER BY title;";
	try {
		$dbCon = getConnection();
		$stmt = $dbCon->prepare($sql);  
		$stmt->bindParam("query", $query);
		$stmt->execute();
		$user = $stmt->fetchAll(PDO::FETCH_ASSOC);  
		$dbCon = null;
		echo json_encode($user); 
	} catch(PDOException $e) {
		echo '{"error":{"text":'. $e->getMessage() .'}}'; 
	}
}

function getConnection() {
	try {
		$db_username = "root";
		$db_password = "debian*11";
		$conn = new PDO('mysql:host=localhost;dbname=capstone_base', $db_username, $db_password);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	} catch(PDOException $e) {
		echo 'ERROR: ' . $e->getMessage();
	}
	return $conn;
}
?>